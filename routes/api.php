<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\MateriController;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/v1', 'as' => 'api.'], function () {
    Route::post('/register', [AuthController::class,'register']);
    Route::post('/login', [AuthController::class,'login']);
});

Route::group(['prefix' => '/v1', 'as' => 'api.', 'middleware' => 'auth:sanctum'], function () {
    Route::prefix('mahasiswa')->group( function(){
        Route::get('/', [MahasiswaController::class,'index']);
        Route::post('/create', [MahasiswaController::class,'create']);
        Route::put('/update', [MahasiswaController::class,'edit']);
        Route::delete('/delete', [MahasiswaController::class,'delete']);
    });

    Route::prefix('kategori')->group( function(){
        Route::get('/', [KategoriController::class,'index']);
        Route::post('/create', [KategoriController::class,'create']);
        Route::put('/update', [KategoriController::class,'update']);
        Route::delete('/delete', [KategoriController::class,'delete']);
    });

    Route::prefix('materi')->group( function(){
        Route::get('/', [MateriController::class,'index']);
        Route::post('/create', [MateriController::class,'create']);
        Route::put('/update', [MateriController::class,'update']);
        Route::delete('/delete', [MateriController::class,'delete']);
    });

    Route::post('/logout', [AuthController::class,'logout']);
});
