<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    use HasFactory;

    public function hasKategori(){
        return $this->belongsTo(Kategori::class,'kategori_id','id');
    }
}
