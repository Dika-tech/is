<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use Illuminate\Support\Facades\Validator;

class MahasiswaController extends Controller
{
    public function index(){
        $mhs = Mahasiswa::get();
        $return['status'] = 'ok';
        $return['data'] = $mhs;
        return response()->json($return);
    }

    public function create(Request $request){
        $mhs = new Mahasiswa();
        $validator = Validator::make($request->all(), [
            'npm' => 'required|string|max:10|min:9|unique:mahasiswas',
            'nama' => 'required|string|min:3',
            'jk' => 'required|string',
            'prodi' => 'required|string',
            'tanggal_dibuat' => 'required'
        ]);

        if ($validator->fails()) {
            $return['status'] = 'error';
      		$return['errors'] = $validator->errors();
            return response()->json($return);
        }else{
        $mhs->npm = $request->npm;
        $mhs->nama = $request->nama;
        $mhs->jk = $request->jk;
        $mhs->prodi = $request->prodi;
        $mhs->created_at = $request->tanggal_dibuat;
        $mhs->save();
        $return['status'] = 'Ok, Data Berhasil ditambahkan';
        return response()->json($return);
        }
    }

    public function edit(Request $request){
        $validator = Validator::make($request->all(), [
            'npm' => 'required|string|max:10|min:9',
            'nama' => 'required|string|min:3',
            'jk' => 'required|string',
            'prodi' => 'required|string',
            'tanggal_dibuat' => 'required'
        ]);

        if ($validator->fails()) {
            $return['status'] = 'error';
      		$return['errors'] = $validator->errors();
            return response()->json($return);
        }else{
            $mhs = Mahasiswa::where('npm',$request->npm)->update([
                'nama' => $request->nama,
                'jk' => $request->jk,
                'prodi' => $request->prodi,
                'updated_at' => $request->tanggal_edit
            ]);
            $return['status'] = 'Ok, Data Berhasil Diperbaharui';
            return response()->json($return);
        }
    }

    public function delete(Request $request){
        $mhs = Mahasiswa::where('npm',$request->npm)->delete();
        $return['status'] = 'Ok, Data Berhasil Dihapus';
        return response()->json($return);
    }
}
