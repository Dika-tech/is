<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;
use Illuminate\Support\Facades\Validator;

class KategoriController extends Controller
{
    public function index(){
        $kategori = Kategori::get();
        $return['status'] = 'Ok';
        $return['data'] = $kategori;
        return response()->json($return);
    }

    public function create(Request $request){
        $kategori = new Kategori();
        $validator = Validator::make($request->all(), [
            'nama_kategori' => 'required|string',
            'deskripsi' => 'required'
        ]);

        if ($validator->fails()) {
            $return['status'] = 'error';
      		$return['errors'] = $validator->errors();
            return response()->json($return);
        }else{
        $gambar = $request->gambar;
        $kategori->nama_kategori = $request->nama_kategori;
        $kategori->deskripsi = $request->deskripsi;
        $kategori->gambar = url('images/'.$request->nama_kategori.".".$gambar->getClientOriginalExtension());
        $kategori->created_at = $request->tanggal_dibuat;
        $kategori->save();
        $gambar->move('images/',$request->nama_kategori.".".$gambar->getClientOriginalExtension());
        $return['status'] = 'Ok, Data Berhasil ditambahkan';
        return response()->json($return);
        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'nama_kategori' => 'required|string',
            'deskripsi' => 'required'
        ]);

        if ($validator->fails()) {
            $return['status'] = 'error';
            $return['errors'] = $validator->errors();
            return response()->json($return);
        }else{
            $gambar = $request->gambar;
            $kategori = Kategori::where('id',$request->id)->update([
                'nama_kategori' => $request->nama_kategori,
                'deskripsi' => $request->deskripsi,
                'gambar' => url('images/'.$request->nama_kategori.".".$gambar->getClientOriginalExtension()),
                'updated_at' => $request->tanggal_diubah
            ]);
            $gambar->move('images/',$request->nama_kategori.".".$gambar->getClientOriginalExtension());
            $return['status'] = 'Ok, Data Berhasil diperbaharui';
            return response()->json($return);
        }
    }

    public function delete(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            $return['status'] = 'error';
            $return['errors'] = $validator->errors();
            return response()->json($return);
        }else{
            $kategori = Kategori::where('id',$request->id)->delete();
            $return['status'] = 'Ok, Data Berhasil dihapus';
            return response()->json($return);
        }
    }
}
