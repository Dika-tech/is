<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthController extends Controller
{
    public function register(Request $request){
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:200|unique:users',
            'password' => 'required|string|min:8|confirmed'
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        $token = $user->createToken('auth-sanctum')->plainTextToken;

        return response()->json([
            'message' => 'Register Succesfully',
            'data' => $user,
            'access_token' => $token,
            'token_type' => 'Bearer'
        ]);
    }

    public function login(Request $request){
        $request->validate([
            'email' => 'required|string|max:200',
            'password' => 'required|string|min:8'
        ]);

        if(!Auth::attempt(
            $request->only('email','password')
        )){
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

        $user = User::where('email',$request->email)->firstOrFail();

        $token = $user->createToken('auth-sanctum')->plainTextToken;

        return response()->json([
            'message' => 'Login successfully',
            'data' => $user,
            'access_token' => $token,
            'token_type' => 'Bearer'
        ]);
    }

    public function delete(Request $request)
    {
        $user = User::where('id',$request->id)->delete();

        return response()->json([
            'message' => 'User has been deleted successfully',
            'data' => $user,
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();

        return response()->json([
            'message' => 'You already logout'
        ]);
    }

    public function all(Request $request)
    {
        $user = User::get();

        return response()->json([
            'message' => 'Data Found successfully',
            'data' => $user
        ]);
    }
}