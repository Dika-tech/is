<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Materi;
use Illuminate\Support\Facades\Validator;

class MateriController extends Controller
{
    public function index(){
        $materi = Materi::with('hasKategori')->get();
        $return['status'] = 'Ok';
        $return['data'] = $materi;
        return response()->json($return);
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'kategori_id' => 'required',
            'kode_materi' => 'required|string|min:3|max:10',
            'nama_materi' => 'required|string|min:3',
            'harga' => 'required'
        ]);

        if ($validator->fails()) {
            $return['status'] = 'error';
      		$return['errors'] = $validator->errors();
            return response()->json($return);
        }else{
            $gambar = $request->gambar;
            $materi = new Materi();
            $materi->kategori_id = $request->kategori_id;
            $materi->kode_materi = $request->kode_materi;
            $materi->nama_materi = $request->nama_materi;
            $materi->gambar = url('images/materi/'.$request->nama_materi.'.'.$gambar->getClientOriginalExtension());
            $materi->deskripsi = $request->deskripsi;
            $materi->harga = $request->harga;
            $materi->views = 0;
            $materi->created_at = $request->tanggal_dibuat;
            $materi->save();

            $gambar->move('images/materi/',$request->nama_materi.".".$gambar->getClientOriginalExtension());
            $return['status'] = 'Ok, Data berhasil ditambahkan';
            return response()->json($return);
        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'kategori_id' => 'required',
            'kode_materi' => 'required|string|min:3|max:10',
            'nama_materi' => 'required|string|min:3',
            'harga' => 'required'
        ]);

        if ($validator->fails()) {
            $return['status'] = 'error';
      		$return['errors'] = $validator->errors();
            return response()->json($return);
        }else{
            $gambar = $request->gambar;
            $materi = Materi::where('id',$request->id)->update([
                'kategori_id' => $request->kategori_id,
                'kode_materi' => $request->kode_materi,
                'nama_materi' => $request->nama_materi,
                'gambar' => url('images/materi/'.$request->nama_materi.'.'.$gambar->getClientOriginalExtension()),
                'deskripsi' => $request->deskripsi,
                'harga' => $request->harga,
                'updated_at' => $request->tanggal_diubah
            ]);
            $gambar->move('images/materi/',$request->nama_materi.".".$gambar->getClientOriginalExtension());
            $return['status'] = 'Ok, Data berhasil diperbaharui';
            return response()->json($return);
        }
    }

    public function delete(Request $request){
        $materi = Materi::where('id',$request->id)->delete();
        $return['status'] = 'Ok, Data Berhasil Dihapus';
        return response()->json($return);
    }
}
